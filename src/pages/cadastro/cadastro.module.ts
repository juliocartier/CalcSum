import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CadastroPage } from './cadastro';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    CadastroPage,
  ],
  imports: [
    TranslateModule,
    IonicPageModule.forChild(CadastroPage),
  ],
})
export class CadastroPageModule {}
