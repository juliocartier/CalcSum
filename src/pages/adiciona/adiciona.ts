import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';
import { AdMobFree, AdMobFreeInterstitialConfig, AdMobFreeBannerConfig} from '@ionic-native/admob-free';

/**
 * Generated class for the AdicionaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-adiciona',
  templateUrl: 'adiciona.html',
})
export class AdicionaPage {

	data = { date:"", nome:"", valor:0 };

  constructor(public navCtrl: NavController, public navParams: NavParams, private sqlite: SQLite, private toast: Toast, private admobFree: AdMobFree) {
    //this.showInterstialAd();
    //this.showBannerAd();

  }

  async showInterstialAd(){
    try {
      const interstitialConfig: AdMobFreeInterstitialConfig = {
        id: 'ca-app-pub-4250164210737037/5687900015',
        isTesting: false,
        autoShow: true
      }

      this.admobFree.interstitial.config(interstitialConfig);

      const result = await this.admobFree.interstitial.prepare();
      console.log("Esta funcionando", result);

    } catch(e) {
      // statements
      console.log(e);
    }
  }

    async showBannerAd(){
    const bannerConfig: AdMobFreeBannerConfig = {
      id: 'ca-app-pub-4250164210737037/5764974957',
      isTesting: false,
      autoShow: true
      //bannerAtTop: true
    }

    this.admobFree.banner.config(bannerConfig);

    try {
      const result = this.admobFree.banner.prepare();
    } catch(e) {
      // statements
      //console.log(e);
      console.error(e);
    }
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdicionaPage');
  }

  saveData() {
    this.sqlite.create({
      name: 'cadastro.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('INSERT INTO expense VALUES(NULL,?,?,?)',[this.data.date,this.data.nome,this.data.valor])
        .then(res => {
          console.log(res);
          this.toast.show('Produto Salvo', '5000', 'center').subscribe(
            toast => {
              this.navCtrl.popToRoot();
            }
          );
        })
        .catch(e => {
          console.log(e);
          this.toast.show(e, '5000', 'center').subscribe(
            toast => {
              console.log(toast);
            }
          );
        });
    }).catch(e => {
      console.log(e);
      this.toast.show(e, '5000', 'center').subscribe(
        toast => {
          console.log(toast);
        }
      );
    });
  }

}
