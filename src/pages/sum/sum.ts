import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, FabContainer } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
/**
 * Generated class for the SumPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sum',
  templateUrl: 'sum.html',
})
export class SumPage {

  objeto_pag : any;
  objeto_dias : any;
  objetos_pagamento : any;
  plano: any;

  calculoTaxas : string = '';	
  parcelas : any;
  maquina : string = '';
  pagamento : string = '';
  recebimento : string = '';
  parcelamento : string = '';
  valorDig : any; 
  lang: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private modal: ModalController, public translate: TranslateService) {
    this.lang = 'pt';
    this.translate.setDefaultLang('pt');
    this.translate.use('pt');
  }

  traducao(lang: any, fab: FabContainer) {
    fab.close();
    //console.log("Sharing in", lang);
    this.translate.use(lang);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SumPage');
  }

   onSelectChange(selectedValue: any) {
    this.plano = selectedValue;
    if (this.plano == 'Acelerado') {
       this.objetos_pagamento = [{pagamento: 'Debito'}, {pagamento: 'Credito'}];    
      } else if(this.plano == 'Economico'){
      	this.objetos_pagamento = [{pagamento: 'Debito'}, {pagamento: 'Credito'}];
      }
  } 

  onRecebimento(selectedValue : any){
  	this.recebimento = selectedValue;
  	if (this.recebimento == 'Debito') {
  		this.objeto_pag = [{parcelas: 1}];    
  	} else if (this.recebimento == 'Credito') {
  		this.objeto_pag = [{parcelas : 1},{parcelas: 2},{parcelas: 3},{parcelas: 4}
    	 ,{parcelas: 5}, {parcelas: 6}, {parcelas: 7}, {parcelas: 8}, {parcelas: 9},
    	 {parcelas: 10}, {parcelas: 11}, {parcelas: 12}];
  	}
  }    
    
  

  openModal(valorDig : any){

    let calculoTaxas = this.calculoTaxas;
    let parcelas = parseFloat(this.parcelas);
    let maquina = this.maquina;
    let plano = this.plano;
    let recebimento = this.recebimento;
    let valor = parseFloat(this.valorDig);
    let lang = this.lang;
    
    let myData = {
      calculoTaxas: calculoTaxas,
      valor: valor,
      maquina: maquina,
      plano : plano,
      parcelas: parcelas,
      recebimento: recebimento,
      lang : lang
    };

    //console.log(myData);
    const myModal = this.modal.create('ModalPage',  myData);

    myModal.present();
  }

}
