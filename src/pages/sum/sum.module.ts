import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SumPage } from './sum';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [
    SumPage,
  ],
  imports: [
    TranslateModule,
    IonicPageModule.forChild(SumPage),
  ],
})
export class SumPageModule {}
