import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AdMobFree, AdMobFreeInterstitialConfig, AdMobFreeBannerConfig} from '@ionic-native/admob-free';
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the ResultadoCartaoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-resultado-cartao',
  templateUrl: 'resultado-cartao.html',
})
export class ResultadoCartaoPage {
	tipoModo : any = this.navParams.get('calculoTaxas');
	valorTotal : any = this.navParams.get('valor');
	tipoPlano : any = this.navParams.get('plano');
	lang: string = this.navParams.get('lang');
	valorDebito : any;
	valorParcela1 : any;
	valorParcela2 : any;
	valorParcela3 : any;
	valorParcela4 : any;
	valorParcela5 : any;
	valorParcela6 : any;
	valorParcela7 : any;
	valorParcela8 : any;
	valorParcela9 : any;
	valorParcela10 : any;
	valorParcela11 : any;
	valorParcela12 : any;
	textoTaxas : any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private admobFree: AdMobFree, public translate: TranslateService) {
  	//this.showBannerAd();
    //this.showInterstialAd();
		this.funcaoPagamentoDetalhado();
		//console.log(this.lang);
	}
	
	switchLanguage() {
    this.translate.use(this.lang);
  }

  async showInterstialAd(){
    try {
      const interstitialConfig: AdMobFreeInterstitialConfig = {
        id: 'ca-app-pub-4250164210737037/5687900015',
        isTesting: false,
        autoShow: true
      }

      this.admobFree.interstitial.config(interstitialConfig);

      const result = await this.admobFree.interstitial.prepare();
      //console.log("Esta funcionando", result);

    } catch(e) {
      // statements
      console.log(e);
    }
  }

    async showBannerAd(){
    const bannerConfig: AdMobFreeBannerConfig = {
      id: 'ca-app-pub-4250164210737037/5764974957',
      isTesting: false,
      autoShow: true
    }

    this.admobFree.banner.config(bannerConfig);

    try {
      const result = this.admobFree.banner.prepare();
    } catch(e) {
      console.error(e);
    }
    
  }

  funcaoPagamentoDetalhado(){
  	let tipo = this.tipoModo;
    let valor = this.valorTotal;
    let plano = this.tipoPlano;


    if (tipo == "Taxas") {
    		if (plano == "Acelerado") {
    			 //parseFloat("" + (0.0378 * 100).toFixed(2))
    			this.textoTaxas = "Você irá receber no:";
    			this.valorDebito = parseFloat("" + (valor - (valor * 0.023)).toFixed(2));
		    	this.valorParcela1 = parseFloat("" + (valor - (valor * 0.046)).toFixed(2));
		    	this.valorParcela2 = parseFloat("" + (valor - (valor * 0.046) - (valor * 0.015)).toFixed(2));
		    	this.valorParcela3 = parseFloat("" + (valor - (valor * 0.046) - (valor * 0.03)).toFixed(2));
		    	this.valorParcela4 = parseFloat("" + (valor - (valor * 0.046) - (valor * 0.045)).toFixed(2));
		    	this.valorParcela5 = parseFloat("" + (valor - (valor * 0.046) - (valor * 0.06)).toFixed(2));
		    	this.valorParcela6 = parseFloat("" + (valor - (valor * 0.046) - (valor * 0.075)).toFixed(2));
		    	this.valorParcela7 = parseFloat("" + (valor - (valor * 0.046) - (valor * 0.09)).toFixed(2));
		    	this.valorParcela8 = parseFloat("" + (valor - (valor * 0.046) - (valor * 0.105)).toFixed(2));
		    	this.valorParcela9 = parseFloat("" + (valor - (valor * 0.046) - (valor * 0.12)).toFixed(2));
		    	this.valorParcela10 = parseFloat("" + (valor - (valor * 0.046) - (valor * 0.135)).toFixed(2));
		    	this.valorParcela11 = parseFloat("" + (valor - (valor * 0.046) - (valor * 0.15)).toFixed(2));
		    	this.valorParcela12 = parseFloat("" + (valor - (valor * 0.046) - (valor * 0.165)).toFixed(2));
		

    		}else if(plano == "Economico"){
    			this.textoTaxas = "Você irá receber no:";
    			this.valorDebito =  parseFloat("" + (valor - (valor * 0.023)).toFixed(2)); 
		    	this.valorParcela1 = parseFloat("" + (valor - (valor * 0.031)).toFixed(2));
		    	this.valorParcela2 = parseFloat("" + (valor - (valor * 0.039)).toFixed(2));
		    	this.valorParcela3 = parseFloat("" + (valor - (valor * 0.039)).toFixed(2));
		    	this.valorParcela4 = parseFloat("" + (valor - (valor * 0.039)).toFixed(2));
		    	this.valorParcela5 = parseFloat("" + (valor - (valor * 0.039)).toFixed(2));
		    	this.valorParcela6 = parseFloat("" + (valor - (valor * 0.039)).toFixed(2));
		    	this.valorParcela7 = parseFloat("" + (valor - (valor * 0.039)).toFixed(2));
		    	this.valorParcela8 = parseFloat("" + (valor - (valor * 0.039)).toFixed(2));
		    	this.valorParcela9 = parseFloat("" + (valor - (valor * 0.039)).toFixed(2));
		    	this.valorParcela10 = parseFloat("" + (valor - (valor * 0.039)).toFixed(2));
		    	this.valorParcela11 = parseFloat("" + (valor - (valor * 0.039)).toFixed(2));
		    	this.valorParcela12 = parseFloat("" + (valor - (valor * 0.039)).toFixed(2));
    		}
    } else if (tipo == "Reverso") {
    		if (plano == "Acelerado") {
    			this.textoTaxas = "Você deverá cobrar no:";
    			this.valorDebito = parseFloat("" + (valor + (valor * 0.02355)).toFixed(2));
		    	this.valorParcela1 = parseFloat("" + (valor + (valor * 0.0482)).toFixed(2));
		    	this.valorParcela2 = parseFloat("" + (valor + (valor * 0.049) + (valor * 0.01595)).toFixed(2));
		    	this.valorParcela3 = parseFloat("" + (valor + (valor * 0.0498) + (valor * 0.03245)).toFixed(2));
		    	this.valorParcela4 = parseFloat("" + (valor + (valor * 0.0506) + (valor * 0.0495)).toFixed(2));
		    	this.valorParcela5 = parseFloat("" + (valor + (valor * 0.05145) + (valor * 0.0671)).toFixed(2));
		    	this.valorParcela6 = parseFloat("" + (valor + (valor * 0.05235) + (valor * 0.0853)).toFixed(2));
		    	this.valorParcela7 = parseFloat("" + (valor + (valor * 0.05325) + (valor * 0.10416)).toFixed(2));
		    	this.valorParcela8 = parseFloat("" + (valor + (valor * 0.0542) + (valor * 0.12367)).toFixed(2));
		    	this.valorParcela9 = parseFloat("" + (valor + (valor * 0.05517) + (valor * 0.1439)).toFixed(2));
		    	this.valorParcela10 = parseFloat("" + (valor + (valor * 0.05615) + (valor * 0.16483)).toFixed(2));
		    	this.valorParcela11 = parseFloat("" + (valor + (valor * 0.0572) + (valor * 0.18657)).toFixed(2));
		    	this.valorParcela12 = parseFloat("" + (valor + (valor * 0.0583) + (valor * 0.20915)).toFixed(2));

    		}else if(plano == "Economico"){
    			this.textoTaxas = "Você deverá cobrar no:";
    			this.valorDebito =  parseFloat("" + (valor + (valor * 0.02355)).toFixed(2)); 
		    	this.valorParcela1 = parseFloat("" + (valor + (valor * 0.032)).toFixed(2));
		    	this.valorParcela2 = parseFloat("" + (valor + (valor * 0.0406)).toFixed(2));
		    	this.valorParcela3 = parseFloat("" + (valor + (valor * 0.0406)).toFixed(2));
		    	this.valorParcela4 = parseFloat("" + (valor + (valor * 0.0406)).toFixed(2));
		    	this.valorParcela5 = parseFloat("" + (valor + (valor * 0.0406)).toFixed(2));
		    	this.valorParcela6 = parseFloat("" + (valor + (valor * 0.0406)).toFixed(2));
		    	this.valorParcela7 = parseFloat("" + (valor + (valor * 0.0406)).toFixed(2));
		    	this.valorParcela8 = parseFloat("" + (valor + (valor * 0.0406)).toFixed(2));
		    	this.valorParcela9 = parseFloat("" + (valor + (valor * 0.0406)).toFixed(2));
		    	this.valorParcela10 = parseFloat("" + (valor + (valor * 0.0406)).toFixed(2));
		    	this.valorParcela11 = parseFloat("" + (valor + (valor * 0.0406)).toFixed(2));
		    	this.valorParcela12 = parseFloat("" + (valor + (valor * 0.0406)).toFixed(2));
    			
    		}
    	}

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResultadoCartaoPage');
  }

}
