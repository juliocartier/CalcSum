import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Screenshot } from '@ionic-native/screenshot';
import { AdMobFree, AdMobFreeInterstitialConfig, AdMobFreeBannerConfig} from '@ionic-native/admob-free';
import { ResultadoCartaoPage } from '../resultado-cartao/resultado-cartao';
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the ModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage {

	tipoModo: string = this.navParams.get('calculoTaxas');
	valorTotal : any = this.navParams.get('valor');
	tipoMaquina : string = this.navParams.get('maquina');
	tipoPlano : string = this.navParams.get('plano');
	qtdParcelas : any = this.navParams.get('parcelas');
  tipoRecebimento : string = this.navParams.get('recebimento');
  lang: string = this.navParams.get('lang');

	taxaCliente : any;
	valorAux : any;
	//qtdParcelas : any;
	valorParcela : any;
	totalImp : any;
	totalParcelamento : any;
	totalTaxas : any;
	total : any;
	valor : any;
	totalCliente : any;
	valorRecebido : any;
	taxasAux : any;
	taxaParcelamentoAux : any;
  	porcentagemVendedor : any;
  	porcentagemMaquina : any;
  	porcentagemCliente : any;
  	totalPorVendedor : any;
  	totalPorCliente : any;
  	totalPorcentagemV : any;
  	screen: any;
  	state: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, private view: ViewController, private screenshot: Screenshot, private admobFree: AdMobFree, public translate: TranslateService) {
      //this.showBannerAd();
      //this.showInterstialAd();
      this.funcaoCalcularSumUP();

     // console.log(this.lang);

    }

  switchLanguage() {
    this.translate.use(this.lang);
  }

    async showInterstialAd(){
    try {
      const interstitialConfig: AdMobFreeInterstitialConfig = {
        id: 'ca-app-pub-4250164210737037/5687900015',
        isTesting: false,
        autoShow: true
      }

      this.admobFree.interstitial.config(interstitialConfig);

      const result = await this.admobFree.interstitial.prepare();
      //console.log("Esta funcionando", result);

    } catch(e) {
      // statements
      console.log(e);
    }
  }

    async showBannerAd(){
    const bannerConfig: AdMobFreeBannerConfig = {
      id: 'ca-app-pub-4250164210737037/5764974957',
      isTesting: false,
      autoShow: true
      //bannerAtTop: true
    }

    this.admobFree.banner.config(bannerConfig);

    try {
      const result = this.admobFree.banner.prepare();
    } catch(e) {
      // statements
      //console.log(e);
      console.error(e);
    }
    
  }

  ResultadoCartao() {
        let tipo = this.tipoModo;
        let valor = parseFloat(this.valorTotal);
        let plano = this.tipoPlano;
        let lang = this.lang;

        let myValor = {
        calculoTaxas: tipo,
        valor: valor,
        plano : plano,
        lang : lang
      };
      //console.log(myValor);
      this.navCtrl.push(ResultadoCartaoPage, myValor); 
   }

    funcaoCalcularSumUP(){

      let tipo = this.tipoModo;
      let valor = this.valorTotal;
      let maquina = this.tipoMaquina;
      let plano = this.tipoPlano;
      let parcelas = this.qtdParcelas;
      let recebimento = this.tipoRecebimento;

      if (tipo == "Taxas") { //Inicio do if do tipo de Taxas
        if (maquina == "SumUP") {//Inicio do if da maquina Sum
          if (plano == "Acelerado") {// Inicio do if do plano
            if (recebimento == "Debito") { //Inicio do if do tipo de Recebimento
              //if (parcelamento == 1) { //Inicio do if do Vendedor
                //Calculos
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = 1;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                this.totalPorCliente = this.taxaCliente;
                this.porcentagemVendedor = (0.023 * 100);
                this.totalPorcentagemV = 0;
                this.totalImp = valor * 0.023;
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.totalPorVendedor = this.porcentagemVendedor;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;

              //Fim do if do Vendedor  
              //}
            } //Fim do if do tipo de recebimento tipo Debito
              else if(recebimento == "Credito"){ //Inicio do if do tipo credito
                if (parcelas == 1) { //Inicio do if da parcela igual a 1
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                this.porcentagemVendedor = (0.046 * 100);
                this.totalPorVendedor = this.porcentagemVendedor;
                this.totalPorCliente = this.taxaCliente;
                this.totalImp = valor * 0.046;
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
               }else if (parcelas == 2) { // Inicio do if da parcela igual a 2

                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                this.porcentagemVendedor = (0.046 * 100);
                this.totalPorcentagemV = (0.015 * 100);
                this.totalPorCliente = 0;
                this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                this.totalImp = valor * 0.046;
                this.totalParcelamento = valor * 0.015;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;

              } // Fim do if da parcela igual a 2
               else if (parcelas == 3) { // Inicio do if da parcela igual a 3

                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                this.porcentagemVendedor = (0.046 * 100);
                this.totalPorcentagemV = (0.03 * 100);
                this.totalPorCliente = 0;
                this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                this.totalImp = valor * 0.046;
                this.totalParcelamento = valor * 0.03;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;

              } // Fim do if da parcela igual a 3
              else if (parcelas == 4) { // Inicio do if da parcela igual a 4

                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                      this.porcentagemVendedor = (0.046 * 100);
                      this.totalPorcentagemV = (0.045 * 100);
                      this.totalPorCliente = 0;
                      this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                this.totalImp = valor * 0.046;
                this.totalParcelamento = valor * 0.045;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;

              } // Fim do if da parcela igual a 4
              else if (parcelas == 5) { // Inicio do if da parcela igual a 5

                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                      this.porcentagemVendedor = (0.046 * 100);
                      this.totalPorcentagemV = (0.06 * 100);
                      this.totalPorCliente = 0;
                      this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                this.totalImp = valor * 0.046;
                this.totalParcelamento = valor * 0.06;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;

              } // Fim do if da parcela igual a 5
              else if (parcelas == 6) { // Inicio do if da parcela igual a 6

                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                      this.porcentagemVendedor = (0.046 * 100);
                      this.totalPorcentagemV = (0.075 * 100);
                      this.totalPorCliente = 0;
                      this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                this.totalImp = valor * 0.046;
                this.totalParcelamento = valor * 0.075;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;

              } // Fim do if da parcela igual a 6
              else if (parcelas == 7) { // Inicio do if da parcela igual a 7

                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                      this.porcentagemVendedor = (0.046 * 100);
                      this.totalPorcentagemV = (0.09 * 100);
                      this.totalPorCliente = 0;
                      this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                this.totalImp = valor * 0.046;
                this.totalParcelamento = valor * 0.09;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;

              } // Fim do if da parcela igual a 7
              else if (parcelas == 8) { // Inicio do if da parcela igual a 8

                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                      this.porcentagemVendedor = (0.046 * 100);
                      this.totalPorcentagemV = (0.105 * 100);
                      this.totalPorCliente = 0;
                      this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                this.totalImp = valor * 0.046;
                this.totalParcelamento = valor * 0.105;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;

              } // Fim do if da parcela igual a 8
              else if (parcelas == 9) { // Inicio do if da parcela igual a 9

                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                      this.porcentagemVendedor = (0.046 * 100);
                      this.totalPorcentagemV = (0.12 * 100);
                      this.totalPorCliente = 0;
                      this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                this.totalImp = valor * 0.046;
                this.totalParcelamento = valor * 0.12;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;

              } // Fim do if da parcela igual a 9
              else if (parcelas == 10) { // Inicio do if da parcela igual a 10

                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                      this.porcentagemVendedor = (0.046 * 100);
                      this.totalPorcentagemV = (0.135 * 100);
                      this.totalPorCliente = 0;
                      this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                this.totalImp = valor * 0.046;
                this.totalParcelamento = valor * 0.135;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;

              } // Fim do if da parcela igual a 10
              else if (parcelas == 11) { // Inicio do if da parcela igual a 11

                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                      this.porcentagemVendedor = (0.046 * 100);
                      this.totalPorcentagemV = (0.15 * 100);
                      this.totalPorCliente = 0;
                      this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                this.totalImp = valor * 0.046;
                this.totalParcelamento = valor * 0.15;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;

              } // Fim do if da parcela igual a 11
              else if (parcelas == 12) { // Inicio do if da parcela igual a 12

                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                      this.porcentagemVendedor = (0.046 * 100);
                      this.totalPorcentagemV = (0.165 * 100);
                      this.totalPorCliente = 0;
                      this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                this.totalImp = valor * 0.046;
                this.totalParcelamento = valor * 0.165;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;

              } // Fim do if da parcela igual a 12
            } //Fim do if do tipo Credito do plano Acelarado

          } //Fim do if do tipo do plano Acelerado
            else if(plano == "Economico"){
              if (recebimento == "Debito") { //Inicio do if do tipo de Recebimento
              //if (parcelamento == 1) { //Inicio do if do Vendedor
                //Calculos
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = 1;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                this.totalPorCliente = this.taxaCliente;
                this.porcentagemVendedor = (0.023 * 100);
                      this.totalPorcentagemV = 0;
                      this.totalImp = valor * 0.023;
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.totalPorVendedor = this.porcentagemVendedor;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;

              //Fim do if do Vendedor  
              //}
            } //Fim do if do tipo de recebimento tipo Debito
            else if(recebimento == "Credito"){//Inicio do if do tipo de Recebimento Credito
              if (parcelas == 1) { //Inicio do if da parcela igual a 1
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                      this.porcentagemVendedor = (0.031 * 100);
                      this.totalPorVendedor = this.porcentagemVendedor;
                      this.totalPorCliente = this.taxaCliente;
                this.totalImp = valor * 0.031;
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
               } //Fim do if da parcela igual a 2
               else if(parcelas == 2){ //Inicio do if da parcela igual a 2
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                      this.porcentagemVendedor = (0.039 * 100);
                      this.totalPorVendedor = this.porcentagemVendedor;
                      this.totalPorCliente = this.taxaCliente;
                this.totalImp = valor * 0.039;
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
               } //Fim do if da parcela igual a 2
               else if(parcelas == 3){ //Inicio do if da parcela igual a 3
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                      this.porcentagemVendedor = (0.039 * 100);
                      this.totalPorVendedor = this.porcentagemVendedor;
                      this.totalPorCliente = this.taxaCliente;
                this.totalImp = valor * 0.039;
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
               } //Fim do if da parcela igual a 3
               else if(parcelas == 4){ //Inicio do if da parcela igual a 4
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                      this.porcentagemVendedor = (0.039 * 100);
                      this.totalPorVendedor = this.porcentagemVendedor;
                      this.totalPorCliente = this.taxaCliente;
                this.totalImp = valor * 0.039;
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
               } //Fim do if da parcela igual a 4
               else if(parcelas == 5){ //Inicio do if da parcela igual a 5
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                      this.porcentagemVendedor = (0.039 * 100);
                      this.totalPorVendedor = this.porcentagemVendedor;
                      this.totalPorCliente = this.taxaCliente;
                this.totalImp = valor * 0.039;
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
               } //Fim do if da parcela igual a 5
               else if(parcelas == 6){ //Inicio do if da parcela igual a 6
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                      this.porcentagemVendedor = (0.039 * 100);
                      this.totalPorVendedor = this.porcentagemVendedor;
                      this.totalPorCliente = this.taxaCliente;
                this.totalImp = valor * 0.039;
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
               } //Fim do if da parcela igual a 6
               else if(parcelas == 7){ //Inicio do if da parcela igual a 7
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                      this.porcentagemVendedor = (0.039 * 100);
                      this.totalPorVendedor = this.porcentagemVendedor;
                      this.totalPorCliente = this.taxaCliente;
                this.totalImp = valor * 0.039;
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
               } //Fim do if da parcela igual a 7
               else if(parcelas == 8){ //Inicio do if da parcela igual a 8
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                      this.porcentagemVendedor = (0.039 * 100);
                      this.totalPorVendedor = this.porcentagemVendedor;
                      this.totalPorCliente = this.taxaCliente;
                this.totalImp = valor * 0.039;
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
               } //Fim do if da parcela igual a 8
               else if(parcelas == 9){ //Inicio do if da parcela igual a 9
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                      this.porcentagemVendedor = (0.039 * 100);
                      this.totalPorVendedor = this.porcentagemVendedor;
                      this.totalPorCliente = this.taxaCliente;
                this.totalImp = valor * 0.039;
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
               } //Fim do if da parcela igual a 9
               else if(parcelas == 10){ //Inicio do if da parcela igual a 10
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                      this.porcentagemVendedor = (0.039 * 100);
                      this.totalPorVendedor = this.porcentagemVendedor;
                      this.totalPorCliente = this.taxaCliente;
                this.totalImp = valor * 0.039;
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
               } //Fim do if da parcela igual a 10
               else if(parcelas == 11){ //Inicio do if da parcela igual a 11
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                      this.porcentagemVendedor = (0.039 * 100);
                      this.totalPorVendedor = this.porcentagemVendedor;
                      this.totalPorCliente = this.taxaCliente;
                this.totalImp = valor * 0.039;
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
               } //Fim do if da parcela igual a 11
               else if(parcelas == 12){ //Inicio do if da parcela igual a 12
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                      this.porcentagemVendedor = (0.039 * 100);
                      this.totalPorVendedor = this.porcentagemVendedor;
                      this.totalPorCliente = this.taxaCliente;
                this.totalImp = valor * 0.039;
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
               } //Fim do if da parcela igual a 12
            } //Fim do if do tipo de Recebimento Credito

          }//Fim do tipo Economico para as Taxas

         } // Fim do if do tipo da maquina Sum
      } //Fim do if do tipo de taxas
        else if (tipo == "Reverso") { //Inicio do if do Reverso
          if (maquina == "SumUP") { //Inicio do if da maquina Sum do Reverso
            if (plano == "Acelerado") {// Inicio do if do plano Acelado
             if (recebimento == "Debito") { //Inicio do if do tipo de Recebimento Debito

                this.taxasAux = valor * 0.02355;
              this.taxaCliente = 0;
              this.valorAux = valor + this.taxasAux;
              this.qtdParcelas = 1;
              this.valorParcela = (this.valorAux/this.qtdParcelas);
              //console.log(this.valorParcela);
                    this.porcentagemVendedor = (0.023 * 100);
                    this.totalPorVendedor = this.porcentagemVendedor;
                    this.totalPorcentagemV = 0;
                    this.totalPorCliente = this.taxaCliente;
              this.totalImp = this.taxasAux;
              this.totalParcelamento = valor * 0;
              this.totalTaxas = this.totalImp + this.totalParcelamento;
              this.total = this.valorAux - this.totalTaxas;
              this.totalCliente = this.valorAux;
             } //Fim do if do tipo de Recebimento Debito
              else if(recebimento == "Credito"){ //Inicio do if do tipo Credito
                if (parcelas == 1) { //Inicio do if da parcela igual a 1
              this.taxasAux = valor * 0.0482;
                
              this.taxaCliente = 0;
              this.valorAux = valor + this.taxasAux;
              this.qtdParcelas = parcelas;
              this.valorParcela = (this.valorAux/this.qtdParcelas);
              this.porcentagemVendedor = (0.046 * 100);
              this.totalPorVendedor = this.porcentagemVendedor;
              this.totalPorCliente = this.taxaCliente;
              this.totalPorcentagemV = 0;
              
              this.totalImp = this.taxasAux;
              this.totalParcelamento = valor * 0;
              this.totalTaxas = this.totalImp + this.totalParcelamento;
              this.total = this.valorAux - this.totalTaxas;
              this.totalCliente = this.valorAux;
             } // Fim do if da parcela igual a 1 do tipo credito
             else if(parcelas == 2){ //Inicio do if do reverso do tipo credito da Parcela igual 2
              this.taxasAux = valor * 0.049;
              this.taxaParcelamentoAux = valor * 0.01595;
              this.porcentagemVendedor = (0.046 * 100);
              this.totalPorcentagemV = 1.5;
              this.totalPorCliente = 0;
              this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;                        
              this.taxaCliente = 0;
              this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
              this.qtdParcelas = parcelas;
              this.valorParcela = (this.valorAux/this.qtdParcelas);
              
              this.totalImp = this.taxasAux;
              this.totalParcelamento = this.taxaParcelamentoAux;
              this.totalTaxas = this.totalImp + this.totalParcelamento;
              this.total = this.valorAux - this.totalTaxas;
              this.totalCliente = this.valorAux;
             } //Fim do if do reverso do tipo credito da Parcela igual 2
             else if(parcelas == 3){ //Inicio do if do reverso do tipo credito da Parcela igual 3
              this.taxasAux = valor * 0.0498;
              this.taxaParcelamentoAux = valor * 0.03245;
              this.porcentagemVendedor = (0.046 * 100);
                        this.totalPorcentagemV = 3.19;
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;                        
              this.taxaCliente = 0;
              this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
              this.qtdParcelas = parcelas;
              this.valorParcela = (this.valorAux/this.qtdParcelas);
              
              this.totalImp = this.taxasAux;
              this.totalParcelamento = this.taxaParcelamentoAux;
              this.totalTaxas = this.totalImp + this.totalParcelamento;
              this.total = this.valorAux - this.totalTaxas;
              this.totalCliente = this.valorAux;
             } //Fim do if do reverso do tipo credito da Parcela igual 3
             else if(parcelas == 4){ //Inicio do if do reverso do tipo credito da Parcela igual 4
              this.taxasAux = valor * 0.0506;
              this.taxaParcelamentoAux = valor * 0.0495;
              this.porcentagemVendedor = (0.046 * 100);
                        this.totalPorcentagemV = 4.5;
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;                        
              this.taxaCliente = 0;
              this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
              this.qtdParcelas = parcelas;
              this.valorParcela = (this.valorAux/this.qtdParcelas);     
              this.totalImp = this.taxasAux;
              this.totalParcelamento = this.taxaParcelamentoAux;
              this.totalTaxas = this.totalImp + this.totalParcelamento;
              this.total = this.valorAux - this.totalTaxas;
              this.totalCliente = this.valorAux;
             } //Fim do if do reverso do tipo credito da Parcela igual 4
             else if(parcelas == 5){ //Inicio do if do reverso do tipo credito da Parcela igual 5
              this.taxasAux = valor * 0.05145;
              this.taxaParcelamentoAux = valor * 0.0671;
              this.porcentagemVendedor = (0.046 * 100);
                        this.totalPorcentagemV = 6;
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;                        
              this.taxaCliente = 0;
              this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
              this.qtdParcelas = parcelas;
              this.valorParcela = (this.valorAux/this.qtdParcelas);     
              this.totalImp = this.taxasAux;
              this.totalParcelamento = this.taxaParcelamentoAux;
              this.totalTaxas = this.totalImp + this.totalParcelamento;
              this.total = this.valorAux - this.totalTaxas;
              this.totalCliente = this.valorAux;
             } //Fim do if do reverso do tipo credito da Parcela igual 5
             else if(parcelas == 6){ //Inicio do if do reverso do tipo credito da Parcela igual 6
              this.taxasAux = valor * 0.05235;
              this.taxaParcelamentoAux = valor * 0.0853;
              this.porcentagemVendedor = (0.046 * 100);
                        this.totalPorcentagemV = 7.5;
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;                        
              this.taxaCliente = 0;
              this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
              this.qtdParcelas = parcelas;
              this.valorParcela = (this.valorAux/this.qtdParcelas);     
              this.totalImp = this.taxasAux;
              this.totalParcelamento = this.taxaParcelamentoAux;
              this.totalTaxas = this.totalImp + this.totalParcelamento;
              this.total = this.valorAux - this.totalTaxas;
              this.totalCliente = this.valorAux;
             } //Fim do if do reverso do tipo credito da Parcela igual 6
             else if(parcelas == 7){ //Inicio do if do reverso do tipo credito da Parcela igual 7
              this.taxasAux = valor * 0.05325;
              this.taxaParcelamentoAux = valor * 0.10416;
              this.porcentagemVendedor = (0.046 * 100);
                        this.totalPorcentagemV = 9;
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;                        
              this.taxaCliente = 0;
              this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
              this.qtdParcelas = parcelas;
              this.valorParcela = (this.valorAux/this.qtdParcelas);     
              this.totalImp = this.taxasAux;
              this.totalParcelamento = this.taxaParcelamentoAux;
              this.totalTaxas = this.totalImp + this.totalParcelamento;
              this.total = this.valorAux - this.totalTaxas;
              this.totalCliente = this.valorAux;
             } //Fim do if do reverso do tipo credito da Parcela igual 7
             else if(parcelas == 8){ //Inicio do if do reverso do tipo credito da Parcela igual 8
              this.taxasAux = valor * 0.0542;
              this.taxaParcelamentoAux = valor * 0.12367;
              this.porcentagemVendedor = (0.046 * 100);
                        this.totalPorcentagemV = 10.5;
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;                        
              this.taxaCliente = 0;
              this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
              this.qtdParcelas = parcelas;
              this.valorParcela = (this.valorAux/this.qtdParcelas);     
              this.totalImp = this.taxasAux;
              this.totalParcelamento = this.taxaParcelamentoAux;
              this.totalTaxas = this.totalImp + this.totalParcelamento;
              this.total = this.valorAux - this.totalTaxas;
              this.totalCliente = this.valorAux;
             } //Fim do if do reverso do tipo credito da Parcela igual 8
             else if(parcelas == 9){ //Inicio do if do reverso do tipo credito da Parcela igual 9
              this.taxasAux = valor * 0.05517;
              this.taxaParcelamentoAux = valor * 0.1439;
              this.porcentagemVendedor = (0.046 * 100);
                        this.totalPorcentagemV = 12;
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;                        
              this.taxaCliente = 0;
              this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
              this.qtdParcelas = parcelas;
              this.valorParcela = (this.valorAux/this.qtdParcelas);     
              this.totalImp = this.taxasAux;
              this.totalParcelamento = this.taxaParcelamentoAux;
              this.totalTaxas = this.totalImp + this.totalParcelamento;
              this.total = this.valorAux - this.totalTaxas;
              this.totalCliente = this.valorAux;
             } //Fim do if do reverso do tipo credito da Parcela igual 9
             else if(parcelas == 10){ //Inicio do if do reverso do tipo credito da Parcela igual 10
              this.taxasAux = valor * 0.05615;
              this.taxaParcelamentoAux = valor * 0.16483;
              this.porcentagemVendedor = (0.046 * 100);
                        this.totalPorcentagemV = 13.5;
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;                        
              this.taxaCliente = 0;
              this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
              this.qtdParcelas = parcelas;
              this.valorParcela = (this.valorAux/this.qtdParcelas);     
              this.totalImp = this.taxasAux;
              this.totalParcelamento = this.taxaParcelamentoAux;
              this.totalTaxas = this.totalImp + this.totalParcelamento;
              this.total = this.valorAux - this.totalTaxas;
              this.totalCliente = this.valorAux;
             } //Fim do if do reverso do tipo credito da Parcela igual 10
             else if(parcelas == 11){ //Inicio do if do reverso do tipo credito da Parcela igual 11
              this.taxasAux = valor * 0.0572;
              this.taxaParcelamentoAux = valor * 0.18657;
              this.porcentagemVendedor = (0.046 * 100);
                        this.totalPorcentagemV = 15;
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;                        
              this.taxaCliente = 0;
              this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
              this.qtdParcelas = parcelas;
              this.valorParcela = (this.valorAux/this.qtdParcelas);     
              this.totalImp = this.taxasAux;
              this.totalParcelamento = this.taxaParcelamentoAux;
              this.totalTaxas = this.totalImp + this.totalParcelamento;
              this.total = this.valorAux - this.totalTaxas;
              this.totalCliente = this.valorAux;
             } //Fim do if do reverso do tipo credito da Parcela igual 11
             else if(parcelas == 12){ //Inicio do if do reverso do tipo credito da Parcela igual 12
              this.taxasAux = valor * 0.0583;
              this.taxaParcelamentoAux = valor * 0.20915;
              this.porcentagemVendedor = (0.046 * 100);
                        this.totalPorcentagemV = 16.5;
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;                        
              this.taxaCliente = 0;
              this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
              this.qtdParcelas = parcelas;
              this.valorParcela = (this.valorAux/this.qtdParcelas);     
              this.totalImp = this.taxasAux;
              this.totalParcelamento = this.taxaParcelamentoAux;
              this.totalTaxas = this.totalImp + this.totalParcelamento;
              this.total = this.valorAux - this.totalTaxas;
              this.totalCliente = this.valorAux;
             } //Fim do if do reverso do tipo credito da Parcela igual 12
              }//Fim do if do tipo Credito  
            } //Fim do if do plano Acelerado
            else if (plano == "Economico") { //Inicio do if do Plano Economico
                if (recebimento == "Debito") { //Inicio do if do tipo de Recebimento Debito
                this.taxasAux = valor * 0.02355;
              this.taxaCliente = 0;
              this.valorAux = valor + this.taxasAux;
              this.qtdParcelas = 1;
              this.valorParcela = (this.valorAux/this.qtdParcelas);
              //console.log(this.valorParcela);
                    this.porcentagemVendedor = (0.023 * 100);
                    this.totalPorVendedor = this.porcentagemVendedor;
                    this.totalPorcentagemV = 0;
                    this.totalPorCliente = this.taxaCliente;
              this.totalImp = this.taxasAux;
              this.totalParcelamento = valor * 0;
              this.totalTaxas = this.totalImp + this.totalParcelamento;
              this.total = this.valorAux - this.totalTaxas;
              this.totalCliente = this.valorAux;
             } //Fim do if do tipo de Recebimento Debito
             else if(recebimento == "Credito"){ //Inicio do if do tipo Credito
              if (parcelas == 1) { //Inicio do if da parcela igual a 1
              this.taxasAux = valor * 0.032;
                
              this.taxaCliente = 0;
              this.valorAux = valor + this.taxasAux;
              this.qtdParcelas = parcelas;
              this.valorParcela = (this.valorAux/this.qtdParcelas);
                        this.porcentagemVendedor = (0.031 * 100);
                        this.totalPorVendedor = this.porcentagemVendedor;
                        this.totalPorCliente = this.taxaCliente;
                        this.totalPorcentagemV = 0;
              
              this.totalImp = this.taxasAux;
              this.totalParcelamento = valor * 0;
              this.totalTaxas = this.totalImp + this.totalParcelamento;
              this.total = this.valorAux - this.totalTaxas;
              this.totalCliente = this.valorAux;
             } // Fim do if da parcela igual a 1 do tipo credito
             else if(parcelas == 2){ //Inicio do if da parcela igual a 2
              this.taxasAux = valor * 0.0406;
                
              this.taxaCliente = 0;
              this.valorAux = valor + this.taxasAux;
              this.qtdParcelas = parcelas;
              this.valorParcela = (this.valorAux/this.qtdParcelas);
                        this.porcentagemVendedor = (0.039 * 100);
                        this.totalPorVendedor = this.porcentagemVendedor;
                        this.totalPorCliente = this.taxaCliente;
                        this.totalPorcentagemV = 0;
              
              this.totalImp = this.taxasAux;
              this.totalParcelamento = valor * 0;
              this.totalTaxas = this.totalImp + this.totalParcelamento;
              this.total = this.valorAux - this.totalTaxas;
              this.totalCliente = this.valorAux;

             } //Fim do if da parcela igual a 2
             else if(parcelas == 3){ //Inicio do if da parcela igual a 3
              this.taxasAux = valor * 0.0406;
                
              this.taxaCliente = 0;
              this.valorAux = valor + this.taxasAux;
              this.qtdParcelas = parcelas;
              this.valorParcela = (this.valorAux/this.qtdParcelas);
                        this.porcentagemVendedor = (0.039 * 100);
                        this.totalPorVendedor = this.porcentagemVendedor;
                        this.totalPorCliente = this.taxaCliente;
                        this.totalPorcentagemV = 0;
              
              this.totalImp = this.taxasAux;
              this.totalParcelamento = valor * 0;
              this.totalTaxas = this.totalImp + this.totalParcelamento;
              this.total = this.valorAux - this.totalTaxas;
              this.totalCliente = this.valorAux;

             } //Fim do if da parcela igual a 3
             else if(parcelas == 4){ //Inicio do if da parcela igual a 4
              this.taxasAux = valor * 0.0406;
                
              this.taxaCliente = 0;
              this.valorAux = valor + this.taxasAux;
              this.qtdParcelas = parcelas;
              this.valorParcela = (this.valorAux/this.qtdParcelas);
                        this.porcentagemVendedor = (0.039 * 100);
                        this.totalPorVendedor = this.porcentagemVendedor;
                        this.totalPorCliente = this.taxaCliente;
                        this.totalPorcentagemV = 0;
              
              this.totalImp = this.taxasAux;
              this.totalParcelamento = valor * 0;
              this.totalTaxas = this.totalImp + this.totalParcelamento;
              this.total = this.valorAux - this.totalTaxas;
              this.totalCliente = this.valorAux;

             } //Fim do if da parcela igual a 4
             else if(parcelas == 5){ //Inicio do if da parcela igual a 5
              this.taxasAux = valor * 0.0406;
                
              this.taxaCliente = 0;
              this.valorAux = valor + this.taxasAux;
              this.qtdParcelas = parcelas;
              this.valorParcela = (this.valorAux/this.qtdParcelas);
                        this.porcentagemVendedor = (0.039 * 100);
                        this.totalPorVendedor = this.porcentagemVendedor;
                        this.totalPorCliente = this.taxaCliente;
                        this.totalPorcentagemV = 0;
              
              this.totalImp = this.taxasAux;
              this.totalParcelamento = valor * 0;
              this.totalTaxas = this.totalImp + this.totalParcelamento;
              this.total = this.valorAux - this.totalTaxas;
              this.totalCliente = this.valorAux;

             } //Fim do if da parcela igual a 5
             else if(parcelas == 6){ //Inicio do if da parcela igual a 6
              this.taxasAux = valor * 0.0406;
                
              this.taxaCliente = 0;
              this.valorAux = valor + this.taxasAux;
              this.qtdParcelas = parcelas;
              this.valorParcela = (this.valorAux/this.qtdParcelas);
                        this.porcentagemVendedor = (0.039 * 100);
                        this.totalPorVendedor = this.porcentagemVendedor;
                        this.totalPorCliente = this.taxaCliente;
                        this.totalPorcentagemV = 0;
              
              this.totalImp = this.taxasAux;
              this.totalParcelamento = valor * 0;
              this.totalTaxas = this.totalImp + this.totalParcelamento;
              this.total = this.valorAux - this.totalTaxas;
              this.totalCliente = this.valorAux;

             } //Fim do if da parcela igual a 6
             else if(parcelas == 7){ //Inicio do if da parcela igual a 7
              this.taxasAux = valor * 0.0406;
                
              this.taxaCliente = 0;
              this.valorAux = valor + this.taxasAux;
              this.qtdParcelas = parcelas;
              this.valorParcela = (this.valorAux/this.qtdParcelas);
                        this.porcentagemVendedor = (0.039 * 100);
                        this.totalPorVendedor = this.porcentagemVendedor;
                        this.totalPorCliente = this.taxaCliente;
                        this.totalPorcentagemV = 0;
              
              this.totalImp = this.taxasAux;
              this.totalParcelamento = valor * 0;
              this.totalTaxas = this.totalImp + this.totalParcelamento;
              this.total = this.valorAux - this.totalTaxas;
              this.totalCliente = this.valorAux;

             } //Fim do if da parcela igual a 7
             else if(parcelas == 8){ //Inicio do if da parcela igual a 8
              this.taxasAux = valor * 0.0406;
                
              this.taxaCliente = 0;
              this.valorAux = valor + this.taxasAux;
              this.qtdParcelas = parcelas;
              this.valorParcela = (this.valorAux/this.qtdParcelas);
                        this.porcentagemVendedor = (0.039 * 100);
                        this.totalPorVendedor = this.porcentagemVendedor;
                        this.totalPorCliente = this.taxaCliente;
                        this.totalPorcentagemV = 0;
              
              this.totalImp = this.taxasAux;
              this.totalParcelamento = valor * 0;
              this.totalTaxas = this.totalImp + this.totalParcelamento;
              this.total = this.valorAux - this.totalTaxas;
              this.totalCliente = this.valorAux;

             } //Fim do if da parcela igual a 8
             else if(parcelas == 9){ //Inicio do if da parcela igual a 9
              this.taxasAux = valor * 0.0406;
                
              this.taxaCliente = 0;
              this.valorAux = valor + this.taxasAux;
              this.qtdParcelas = parcelas;
              this.valorParcela = (this.valorAux/this.qtdParcelas);
                        this.porcentagemVendedor = (0.039 * 100);
                        this.totalPorVendedor = this.porcentagemVendedor;
                        this.totalPorCliente = this.taxaCliente;
                        this.totalPorcentagemV = 0;
              
              this.totalImp = this.taxasAux;
              this.totalParcelamento = valor * 0;
              this.totalTaxas = this.totalImp + this.totalParcelamento;
              this.total = this.valorAux - this.totalTaxas;
              this.totalCliente = this.valorAux;

             } //Fim do if da parcela igual a 9
             else if(parcelas == 10){ //Inicio do if da parcela igual a 10
              this.taxasAux = valor * 0.0406;
                
              this.taxaCliente = 0;
              this.valorAux = valor + this.taxasAux;
              this.qtdParcelas = parcelas;
              this.valorParcela = (this.valorAux/this.qtdParcelas);
                        this.porcentagemVendedor = (0.039 * 100);
                        this.totalPorVendedor = this.porcentagemVendedor;
                        this.totalPorCliente = this.taxaCliente;
                        this.totalPorcentagemV = 0;
              
              this.totalImp = this.taxasAux;
              this.totalParcelamento = valor * 0;
              this.totalTaxas = this.totalImp + this.totalParcelamento;
              this.total = this.valorAux - this.totalTaxas;
              this.totalCliente = this.valorAux;

             } //Fim do if da parcela igual a 10
             else if(parcelas == 11){ //Inicio do if da parcela igual a 11
              this.taxasAux = valor * 0.0406;
                
              this.taxaCliente = 0;
              this.valorAux = valor + this.taxasAux;
              this.qtdParcelas = parcelas;
              this.valorParcela = (this.valorAux/this.qtdParcelas);
                        this.porcentagemVendedor = (0.039 * 100);
                        this.totalPorVendedor = this.porcentagemVendedor;
                        this.totalPorCliente = this.taxaCliente;
                        this.totalPorcentagemV = 0;
              
              this.totalImp = this.taxasAux;
              this.totalParcelamento = valor * 0;
              this.totalTaxas = this.totalImp + this.totalParcelamento;
              this.total = this.valorAux - this.totalTaxas;
              this.totalCliente = this.valorAux;

             } //Fim do if da parcela igual a 11
             else if(parcelas == 12){ //Inicio do if da parcela igual a 12
              this.taxasAux = valor * 0.0406;
                
              this.taxaCliente = 0;
              this.valorAux = valor + this.taxasAux;
              this.qtdParcelas = parcelas;
              this.valorParcela = (this.valorAux/this.qtdParcelas);
                        this.porcentagemVendedor = (0.039 * 100);
                        this.totalPorVendedor = this.porcentagemVendedor;
                        this.totalPorCliente = this.taxaCliente;
                        this.totalPorcentagemV = 0;
              
              this.totalImp = this.taxasAux;
              this.totalParcelamento = valor * 0;
              this.totalTaxas = this.totalImp + this.totalParcelamento;
              this.total = this.valorAux - this.totalTaxas;
              this.totalCliente = this.valorAux;

             } //Fim do if da parcela igual a 12
             } //Fim do if do tipo Credito
            } // Fim do if do Plano Economico
          }//Fim do if da maquina Sum do Reverso
        } //Fim do if do Reverso
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalPage');
  }

  reset() {
    var self = this;
    setTimeout(function(){ 
      self.state = false;
    }, 1000);
  }

  screenShot() {
    this.screenshot.save('jpg', 80).then(res => {
      this.screen = res.filePath;
      this.state = true;
      this.reset();
    });
  } 

   closeModal(){
  	this.view.dismiss();
  }

}
