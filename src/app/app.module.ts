import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { SumPageModule } from '../pages/sum/sum.module';
import { Screenshot } from '@ionic-native/screenshot';
import { CadastroPageModule } from '../pages/cadastro/cadastro.module';
import { AdicionaPageModule } from '../pages/adiciona/adiciona.module';
import { EditarPageModule } from '../pages/editar/editar.module';
import { ResultadoCartaoPageModule} from '../pages/resultado-cartao/resultado-cartao.module';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AdMobFree } from '@ionic-native/admob-free';
import { SQLite } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage
  ],
  imports: [
    BrowserModule,
    SumPageModule,
    CadastroPageModule,
    AdicionaPageModule,
    EditarPageModule,
    ResultadoCartaoPageModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    HttpModule,
    TranslateModule.forRoot({
      loader : {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Screenshot,
    AdMobFree,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SQLite,
    Toast 
  ]
})
export class AppModule {}
