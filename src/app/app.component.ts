import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { SumPage } from '../pages/sum/sum';
import { CadastroPage } from '../pages/cadastro/cadastro';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = SumPage;

  pages: Array<{title: string, component: any}>;
  pages2 : any;

  
  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Calc Tarifas SumUP', component: SumPage },
      { title: 'Cadastrar Produto', component: CadastroPage},
      { title: 'Ajuda', component: HomePage },
      { title: 'Contato', component: ListPage }
    ];

      this.pages2 = {
        SumPage: SumPage,
        CadastroPage: CadastroPage,
        //AntecipacaoPage: AntecipacaoPage,
        HomePage: HomePage,
        ListPage: ListPage
    }

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
